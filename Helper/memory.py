import psutil
import sys
import os
import json

def toJSON(data):
	return json.dumps(data)

def tupleToDict(data,keys):
	
	datadict = {}
	j = 0
	for i in keys:
		datadict[i] = data[j]
		j = j + 1
	return datadict

if(len(sys.argv)>1):
	# swap
	if(sys.argv[1] == '1'):
		keys = ['total', 'used', 'free', 'percent', 'sin', 'sout']
		data = psutil.swap_memory()
		data = tupleToDict(data,keys)
	# virtual memory
	if(sys.argv[1] == '2'):
		keys = ['total', 'available', 'percent', 'used', 'free', 'active','inactive','buffers','cached','shared','slab']
		data = psutil.virtual_memory()
		data = tupleToDict(data,keys)
print(toJSON(data))