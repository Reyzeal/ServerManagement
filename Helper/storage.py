import psutil
import sys
import os
import json

def toJSON(data):
	return json.dumps(data)
def tupleToDict(data,keys):
	
	datadict = {}
	j = 0
	for i in keys:
		datadict[i] = data[j]
		j = j + 1
	return datadict

if(len(sys.argv)>1):
	# berdasarkan mount point
	keys = ['total', 'used', 'free', 'percent']
	data = psutil.disk_usage(sys.argv[1])
	data = tupleToDict(data,keys)
else:
	keys = ['device', 'mountpoint', 'fstype', 'opts']
	data = psutil.disk_partitions()
	datadict = []
	for i in data:
		datadict.append(tupleToDict(i,keys))
	data = datadict 

print(toJSON(data))