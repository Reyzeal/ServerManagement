<?php

namespace Reyzeal;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class ServerManagement{
	public static $directory = '../vendor/reyzeal/server-management/Helper/';

	// Return json
	public static function memoryInfo($select = 1){
		$result = ServerManagement::process(array('python3',self::$directory."/memory.py",$select));
		return $result;
	}
	// Return json
	public static function storageInfo($mount){
		$result = ServerManagement::process(array('python3',self::$directory."/storage.py",$mount));
		return $result;
	}

	public static function diskInfo(){
			
	}
	public static function cpuUsage(){
		$result = ServerManagement::process(array('python3',self::$directory."/cpu.py"));
		return $result;
	}

	public static function makePartition($name,$size){
		return ServerManagement::process('python3 ../vendor/reyzeal/server-management/Hosting/partition.py '.config('server.password')." make $name $size");
	}

	public static function mountPartition($name){
		return ServerManagement::process('python3 ../vendor/reyzeal/server-management/Hosting/partition.py '.config('server.password')." mount $name");
	}

	public static function umountPartition($name){
		return ServerManagement::process('python3 ../vendor/reyzeal/server-management/Hosting/partition.py '.config('server.password')." umount $name");
	}
	public static function destroyPartition($name){
		return ServerManagement::process('python3 ../vendor/reyzeal/server-management/Hosting/partition.py '.config('server.password')." destroy $name");
	}
	public static function resizePartition($name,$size){
		return ServerManagement::process('python3 ../vendor/reyzeal/server-management/Hosting/partition.py '.config('server.password')." resize $name $size");
	}

	public static function process($array){
		$Process = new Process($array);
		$Process -> run();
		if (!$Process->isSuccessful()) {
		    throw new ProcessFailedException($Process);
		}
		return $Process->getOutput();
	}
}