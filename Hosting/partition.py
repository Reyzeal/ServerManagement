import ptyprocess
import sys
import re

if len(sys.argv) < 2:
	print(sys.argv)
	exit()

def execute(arr,printer=True,bash=False):
	p = ptyprocess.PtyProcessUnicode.spawn(arr)
	if arr[0] == 'sudo':
		p.write('%s\n' % (sys.argv[1]))
		p.readline()
		p.readline()
	value = ''
	

	while(1):
	    try:
	        value = '%s%s' % (value,p.read())
	        if printer:
	        	print(p.read())
	    except EOFError:
	        break
	p.close()
	return value

if sys.argv[2] == 'make':
	execute(['sudo','dd','if=/dev/zero','of=/virtualdisks/%s.ext3' % (sys.argv[3]), 'bs=%s' %(sys.argv[4]),'count=1'])
	execute(['sudo','mkfs.ext3','/virtualdisks/%s.ext3' % (sys.argv[3])])
	execute(['sudo','mkdir','/mnt/%s' % (sys.argv[3])])
	execute(['sudo','chown','-R','reyzeal','/mnt/%s' % (sys.argv[3])])
	pass
if sys.argv[2] == 'mount':
	img = '/virtualdisks/%s.ext3' % (sys.argv[3])
	mount_point = '/mnt/%s' % (sys.argv[3])
	
	execute(['sudo','mount','-o','loop,rw,usrquota,grpquota',img,mount_point],False)
	
	data = execute(['blkid',img],False)
	uuid = re.search('UUID="([\w\d-]+)"',data).group(1)
	data = execute(['cat','/etc/fstab'],False)

	comment = '# Hosting %s\n' % (mount_point)
	addon = 'UUID=%s %s ext3 loop,rw,usrquota,grpquota 0 0\n' % (uuid,mount_point)
	data = '%s%s%s' % (data,comment,addon)
	data = re.sub('[\']','\\\'',data)
	data = re.sub('[\"]','\\\"',data)
	print(data,'<hr>')
	execute(['echo','"%s"' % (data),'>','/etc/fstab'])
	execute(['sudo','mount','-a'])
	data = execute(['cat','/etc/fstab'],False)
	for i in data:
		if i == '\n':
			print('<br>')
		print(i,end='')
	pass
	
if sys.argv[2] == 'umount':
	img = '/virtualdisks/%s.ext3' % (sys.argv[3])
	mount_point = '/mnt/%s' % (sys.argv[3])

	data = execute(['blkid',img],False)
	uuid = re.search('UUID="([\w\d-]+)"',data).group(1)
	data = execute(['cat','/etc/fstab'],False)

	comment = '# Hosting %s\n' % (mount_point)
	addon = 'UUID=%s %s ext3 loop,rw,usrquota,grpquota 0 0\n' % (uuid,mount_point)
	data = re.sub(comment,'',data)
	data = re.sub(addon,'',data)
	data = re.sub('[\']','\\\'',data)
	data = re.sub('[\"]','\\\"',data)
	for i in data:
		if i == '\n':
			print('<br>')
		if i == ' ':
			print('&nbsp;')
		print(i,end='')
	pass
	execute(['sudo','echo','"%s"' %data,'>','/etc/fstab'],False)
	execute(['sudo','umount','/mnt/%s' % (sys.argv[3])],False)
	execute(['sudo','mount','-a'],False)
	
	
	pass
if sys.argv[2] == 'destroy':
	execute(['sudo','umount','/mnt/%s' % (sys.argv[3])])
	execute(['sudo','rm','/virtualdisks/%s.ext3' % (sys.argv[3])])
	execute(['sudo','rm','-R','/mnt/%s' % (sys.argv[3])])
	pass
if sys.argv[2] == 'resize':
	execute(['sudo','e2fsck','-f','/virtualdisks/%s.ext3' % (sys.argv[3])])
	execute(['sudo','resize2fs','-p','/virtualdisks/%s.ext3' % (sys.argv[3]),sys.argv[4]])
	pass
# x = subprocess.Popen(['sudo','-u','reyzeal','-S','sudo','fdisk','-l'],stdout=subprocess.PIPE,stdin=subprocess.PIPE, stderr=subprocess.PIPE)
# # x.stdin.write(b'2log10=2\n');
# out, err = x.communicate(input='2log10=2\n'.encode())
# print(out,err)