import ptyprocess
import sys

if len(sys.argv) < 2:
	print(sys.argv)
	exit()

def execute(arr):
	p = ptyprocess.PtyProcessUnicode.spawn(arr)
	p.write('%s\n' % (sys.argv[1]))
	p.readline()
	p.readline()
	while(1):
	    try:
	        print(p.read())
	    except EOFError:
	        break
	p.close()

if sys.argv[2] == 'make':
	execute(['sudo','dd','if=/dev/zero','of=/virtualdisks/%s.ext3' % (sys.argv[3]), 'bs=64K','count=1'])
	execute(['sudo','mkfs.ext3','/virtualdisks/%s.ext3' % (sys.argv[3])])
	execute(['sudo','mkdir','/mnt/%s' % (sys.argv[3])])
	execute(['sudo','chown','-R','reyzeal','/mnt/%s' % (sys.argv[3])])
	pass
if sys.argv[2] == 'destroy':
	execute(['sudo','umount','/mnt/%s' % (sys.argv[3])])
	execute(['sudo','rm','/virtualdisks/%s.ext3' % (sys.argv[3])])
	execute(['sudo','rm','-R','/mnt/%s' % (sys.argv[3])])
	pass
# x = subprocess.Popen(['sudo','-u','reyzeal','-S','sudo','fdisk','-l'],stdout=subprocess.PIPE,stdin=subprocess.PIPE, stderr=subprocess.PIPE)
# # x.stdin.write(b'2log10=2\n');
# out, err = x.communicate(input='2log10=2\n'.encode())
# print(out,err)